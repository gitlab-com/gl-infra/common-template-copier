# `common-template-copier`

This repository contains a project template that can be used to initialize new GitLab Infrastructure projects. It relies on the [`copier` project template generator](https://copier.readthedocs.io/en/latest/).

## Creating a new project

Start off by setting your project name:

```shell
project_name=NAME_YOUR_PROJECT
```

```shell
# Install the copier python CLI
pip3 install --upgrade copier

# Generate a new project using this template. This will prompt you for some
# more information
copier copy https://gitlab.com/gitlab-com/gl-infra/common-template-copier.git "${project_name:?}" --overwrite --trust

cd $project_name
git init
git add .
git commit -m "Initial commit: project generated from https://gitlab.com/gitlab-com/gl-infra/common-template-copier"

# Push to create a new project on GitLab.com
git push --set-upstream git@gitlab.com:gitlab-com/gl-infra/$project_name.git main
git remote add origin git@gitlab.com:gitlab-com/gl-infra/$project_name.git
git fetch
git symbolic-ref refs/remotes/origin/HEAD refs/remotes/origin/main
```

## Updating the copier template in an existing project

Run the following, after installing copier, see above for how to install.

```shell
copier update --trust --skip-answered
```

If you want to update to a specific ref, while working on changes to
the template you can run:

```shell
copier update --trust --skip-answered -r <branch-name>
```

## Hacking on common-template-copier

### Preparing your Environment

1. Follow the developer setup guide at <https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/docs/developer-setup.md>.
1. Clone this project
1. Run `scripts/prepare-dev-env.sh`
