#!/usr/bin/env sh

file="$1"

entry="
[{Makefile,**.mk}]
# Use tabs for indentation (Makefiles require tabs)
indent_style = tab
"

if ! grep -q "Makefile" "$file"; then
  echo "$entry" >>"$file"
fi
