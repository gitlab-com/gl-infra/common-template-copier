#!/usr/bin/env bash

curl https://mise.run | sh
eval "$(~/.local/bin/mise activate bash)"

mise install || true

git add --all .
pre-commit run || true
