#!/usr/bin/env bash

## Attempt to cleanup formatted YAML so that it matches the
tmpdir=$(mktemp -d)
python3 -m venv "$tmpdir"

# shellcheck source=/dev/null
source "$tmpdir/bin/activate"

pip3 install yamlfix

export YAMLFIX_LINE_LENGTH=120
export YAMLFIX_EXPLICIT_START=false
export YAMLFIX_WHITELINES=1
export YAMLFIX_EXPLICIT_START=false
export YAMLFIX_SEQUENCE_STYLE=keep_style
export YAMLFIX_preserve_quotes=true

IFS=$'\n' read -r -d '' -a modified_files < <(
  git diff --name-status |
    awk 'match($2, "\.ya?ml$") {print $2}' |
    grep -Ev '.copier-answers.yml|.gitlab-ci-asdf-versions.yml'
)

if [[ ${#modified_files[@]} -gt 0 ]]; then
  echo yamlfix "${modified_files[@]}"
  yamlfix "${modified_files[@]}"
else
  echo "no files to fix"
fi
