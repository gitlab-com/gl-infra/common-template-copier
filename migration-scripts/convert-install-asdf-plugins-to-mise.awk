#!/usr/bin/env -S awk -f

BEGIN {
  print "[settings]"
  print "legacy_version_file = false"
  print ""
  print "# [tools]"
  print "# Please continue to use `.tool-versions` to specify tools, rather than adding them directly here."
  print ""
  print "[plugins]"
  print "# Use this section to configure the source of any custom mise/asdf plugin"
}

# Match install_plugin with a URL parameter
/^install_plugin/ && NF==3 {
  printf "%s = '%s'\n", $2,$3
}
