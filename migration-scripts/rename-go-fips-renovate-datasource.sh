#!/usr/bin/env bash

# Due to https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/issues/7886
# we need to update all Renovate references to registry.gitlab.com/gitlab-org/gitlab-runner/go-fips
# and replace it with an infra reference.
# This fixes the problem across all repos.

set -euo pipefail
IFS=$'\n\t'

tmpdir=$(mktemp -d)
trap cleanup EXIT

main() {
  sed -E 's@^(golang.* )# datasource=docker depName=registry.gitlab.com/gitlab-org/gitlab-runner/go-fips@\1# datasource=docker depName=registry.gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images/golang-fips@' .tool-versions >"$tmpdir/.tool-versions"
  mv "$tmpdir/.tool-versions" .
}

cleanup() {
  rm -rf "$tmpdir"
}

main "$@"
