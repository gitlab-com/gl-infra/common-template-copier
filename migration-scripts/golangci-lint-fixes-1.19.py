#!/usr/bin/env python3

import sys

import ruamel.yaml


file_changes = False


def has_file_changes():
    global file_changes
    file_changes = True


yaml = ruamel.yaml.YAML(typ="rt")
yaml.preserve_quotes = True
yaml.indent(mapping=2, sequence=4, offset=2)
config = yaml.load(open(".golangci.yaml"))

if "issues" in config:
    if "exclude-rules" in config["issues"]:
        exclude_rules = config["issues"]["exclude-rules"]

        if not any(
            exclude_rule["path"] == "_test.go$"
            and exclude_rule["source"] == "^func (Benchmark|Example|Fuzz|Test)[A-Z]"
            for exclude_rule in exclude_rules
        ):
            has_file_changes()
            new_rule = ruamel.yaml.CommentedMap(
                {
                    "path": "_test.go$",
                    "source": "^func (Benchmark|Example|Fuzz|Test)[A-Z]",
                    "linters": ["funlen"],
                }
            )
            new_rule.yaml_set_start_comment(
                "Exempt test functions from function name length restrictions.",
                indent=4,
            )
            exclude_rules.append(new_rule)

if "linters-settings" in config:
    linters_settings = config["linters-settings"]
    if "govet" in linters_settings:
        govet = linters_settings["govet"]
        if "disable" in govet:
            disable = govet["disable"]
            if not "shadow" in disable:
                has_file_changes()
                disable.append("shadow")

# Only write the file if there are going to be changes...
if file_changes:
    with open(".golangci.yaml", "w") as f:
        yaml.dump(config, f)
