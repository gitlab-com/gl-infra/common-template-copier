#!/usr/bin/env python3

import sys
import json

file_changes = False


def has_file_changes():
    global file_changes
    file_changes = True


TRUNCATED_VERSION_PRESET = (
    "gitlab>gitlab-com/gl-infra/common-ci-tasks:renovate-truncated-versions"
)

with open("renovate.json", "r") as config_file_r:
    renovate_config = json.load(config_file_r)
    extends = renovate_config["extends"]

    if not any(
        extend == TRUNCATED_VERSION_PRESET
        or extend.startswith(TRUNCATED_VERSION_PRESET + "#")
        for extend in extends
    ):
        has_file_changes()
        extends.append(
            "gitlab>gitlab-com/gl-infra/common-ci-tasks:renovate-truncated-versions"
        )

    # Only write the file if there are going to be changes...
    if file_changes:
        with open("renovate.json", "w") as config_file_w:
            pretty_json = json.dumps(renovate_config, sort_keys=False, indent=2)
            config_file_w.write(pretty_json + "\n")
