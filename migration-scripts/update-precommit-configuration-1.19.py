#!/usr/bin/env python3

import sys
import ruamel.yaml


def get_common_ci_tasks_version():
    yaml = ruamel.yaml.YAML()
    gitlab_ci = yaml.load(open(".gitlab-ci.yml"))
    includes = gitlab_ci["include"]
    for i in includes:
        if "project" in i and i["project"] == "gitlab-com/gl-infra/common-ci-tasks":
            return i["ref"]


# Load the common_ci revision
common_ci_rev = get_common_ci_tasks_version()
file_changes = False


def has_file_changes():
    global file_changes
    file_changes = True


def remove_repo(config, url):
    modified = False

    repos = config["repos"]
    for i, e in reversed(list(enumerate(config["repos"]))):
        if e["repo"] == url:
            modified = True
            has_file_changes()
            del repos[i]

    return modified


def remove_local_shellcheck(config):
    modified = False

    repos = config["repos"]
    for i, e in reversed(list(enumerate(config["repos"]))):
        if e["repo"] == "local":
            hooks = e["hooks"]
            for j, f in reversed(list(enumerate(hooks))):
                if f["id"] == "shellcheck":
                    modified = True
                    has_file_changes()
                    del hooks[j]

            # No more hooks? Delete the whole repo
            if not hooks:
                file_changes = True
                del repos[i]

    return modified


def add_repo_hook(config, url, rev, id):
    repos = config["repos"]
    for i, e in list(enumerate(config["repos"])):
        if e["repo"] == url:
            hooks = e["hooks"]
            if not any(hook["id"] == id for hook in hooks):
                has_file_changes()
                hooks.append({"id": id})
            return

    # Not found, add repo from scratch...
    has_file_changes()
    repo = {"repo": url, "rev": rev, "hooks": [{"id": id}]}
    repos.append(repo)


def add_common_ci_hook(config, id):
    add_repo_hook(
        config,
        "https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks",
        common_ci_rev,
        id,
    )


yaml = ruamel.yaml.YAML(typ="rt")
yaml.indent(mapping=2, sequence=4, offset=2)
config = yaml.load(open(".pre-commit-config.yaml"))

# We no longer run checkov in pre-commit as it's flaky and leads to a lot of problems
checkov = remove_repo(config, "https://github.com/bridgecrewio/checkov.git")

# Remove these repos as we're using curated pre-commit hooks now
gitleaks = remove_repo(config, "https://github.com/zricethezav/gitleaks")

terraform = remove_repo(config, "https://github.com/antonbabenko/pre-commit-terraform")
yamllint = remove_repo(config, "https://github.com/adrienverge/yamllint.git")
shfmt = remove_repo(config, "https://github.com/scop/pre-commit-shfmt")
golang = remove_repo(config, "https://github.com/dnephin/pre-commit-golang")
golangci = remove_repo(config, "https://github.com/golangci/golangci-lint")

remove_repo(config, "https://github.com/compilerla/conventional-pre-commit")
gitlint = remove_repo(config, "https://github.com/jorisroovers/gitlint")

shellcheck = remove_local_shellcheck(config)

if shellcheck:
    add_common_ci_hook(config, "shellcheck")

if shfmt:
    add_common_ci_hook(config, "shfmt")

add_common_ci_hook(config, "update-asdf-version-variables")

if gitlint:
    add_common_ci_hook(config, "gitlint")

add_common_ci_hook(config, "editorconfig")

if yamllint:
    add_common_ci_hook(config, "yamllint")

if golang:
    add_common_ci_hook(config, "go-fmt")
    add_common_ci_hook(config, "go-imports")
    add_common_ci_hook(config, "go-mod-tidy")
    add_common_ci_hook(config, "go-test")

if golangci:
    add_common_ci_hook(config, "golangci-lint")

# Always add gitleaks!
add_common_ci_hook(config, "gitleaks")

if terraform:
    add_common_ci_hook(config, "terraform-fmt")
    add_common_ci_hook(config, "terraform-tflint")
    add_common_ci_hook(config, "terra-transformer-validate-roots")

# Only write the file if there are going to be changes...
if file_changes:
    with open(".pre-commit-config.yaml", "w") as f:
        yaml.dump(config, f)
