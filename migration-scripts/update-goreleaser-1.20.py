#!/usr/bin/env python3

import sys

import ruamel.yaml

file_changes = False


def has_file_changes():
    global file_changes
    file_changes = True


yaml = ruamel.yaml.YAML(typ="rt")
yaml.preserve_quotes = True
yaml.indent(mapping=2, sequence=4, offset=2)
config = yaml.load(open(".goreleaser.yml"))

if not "docker_signs" in config:
    has_file_changes()
    config["docker_signs"] = [
        {
            "id": "keyless_cosign",
            "cmd": "cosign",
            "args": ["sign", "${artifact}", "--yes"],
            "artifacts": "all",
            "output": True,
        }
    ]
    config.yaml_set_comment_before_after_key(
        "docker_signs", "\nhttps://goreleaser.com/customization/docker_sign/"
    )

if not "signs" in config:
    has_file_changes()
    config["signs"] = [
        {
            "id": "keyless_cosign",
            "cmd": "cosign",
            "args": [
                "sign-blob",
                "--output-signature=${signature}",
                "--bundle=${certificate}",
                "${artifact}",
                "--yes",
            ],
            "output": True,
            "artifacts": "archive",
            "signature": '{{ trimsuffix (trimsuffix .Env.artifact ".zip") ".tar.gz" }}.sig',
            "certificate": '{{ trimsuffix (trimsuffix .Env.artifact ".zip") ".tar.gz" }}.bundle',
        }
    ]
    config.yaml_set_comment_before_after_key(
        "signs", "\nhttps://goreleaser.com/customization/sign/"
    )

if not "sboms" in config:
    has_file_changes()
    config["sboms"] = [{"artifacts": "archive"}]
    config.yaml_set_comment_before_after_key(
        "sboms", "\nPublish syft SBOMs\nhttps://goreleaser.com/customization/sbom/"
    )

# Only write the file if there are going to be changes...
if file_changes:
    with open(".goreleaser.yml", "w") as f:
        yaml.dump(config, f)
