#!/usr/bin/env bash

set -euo pipefail

script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)

# shellcheck source=./test-lib.sh
source "${script_dir}/test-lib.sh"

test_template "${script_dir}/terraform-answers.yml"
