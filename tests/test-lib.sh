# shellcheck shell=bash

test_template() {
  local answers_file=$1
  local script_dir
  script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)

  local template_location
  if [[ -n ${CI_PROJECT_PATH-} ]]; then
    template_location="https://gitlab.com/${CI_PROJECT_PATH}.git"
  else
    template_location="$(rp "${script_dir}/..")"
  fi

  output_dir=$(mktemp -d)
  trap 'rm -rf "${output_dir}"' EXIT

  # copier will use the most recent tag by default
  local git_ref
  if [[ -n ${CI_COMMIT_SHA-} ]]; then
    git_ref="${CI_COMMIT_SHA}"
  else
    git_ref=$(git -C "${script_dir}/.." rev-parse HEAD)
  fi

  # Answers file must be relative
  answers_file=$(rp --relative-to="${output_dir}" "${answers_file}")

  cd "${output_dir}" || exit 1

  copier copy --force --vcs-ref "${git_ref}" --answers-file "${answers_file}" "${template_location}" --trust .

  git -c init.defaultBranch=main init
  git checkout -b "initial-commit"
  git add .

  mise install
  eval "$(mise env --shell bash)"
  pre-commit run -a || {
    git diff
    exit 1
  }
}

# Install coreutils to test on a mac
rp() {
  if (command -v grealpath >/dev/null); then
    grealpath "$@"
  else
    realpath "$@"
  fi
}
